///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
////////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "palila.hpp"


using namespace std;

namespace animalfarm {

Palila::Palila( string newPlace, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all fishs are the same gender (this is a has-a relationship)
   species = "Loxioides bailleui";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
   isMigratory = 0;       /// An is-a relationship, so it's safe to hardcode.  All fishs have the same gestation period.

   place = newPlace;
}



/// Print our Fish and name first... then print whatever information Mammal holds.
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << place << "]" << endl;
   Bird::printInfo();
}



} // namespace animalfarm



