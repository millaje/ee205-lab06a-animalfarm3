///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
//#include <iostream>
//#include <random>
using namespace std;
#include <string>
#include <cstdlib>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN };  /// @todo Add more colors

class Animal {
public: 
      Animal();
      ~Animal();

public:
      enum Gender gender;
	   string      species;

	   virtual const string speak() = 0;
	
	   void printInfo();

	   string colorName  (enum Color color);
	   string genderName (enum Gender gender);
   
   static const Gender  getRandomGender();
   static const Color   getRandomColor();
   static const bool    getRandomBool();
   static const float   getRandomWeight(const float from, const float to);
   static const string  getRandomName();
};

class AnimalFactory{
   public:
      static Animal* getRandomAnimal();
};


} // namespace animalfarm
