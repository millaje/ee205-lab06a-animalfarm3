///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   2 Mar 2021 
///////////////////////////////////////////////////////////////////////////////
/*
#include <iostream>
#include <array>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

#define MAX_LENGTH (9)
std::string randomName() {
   srand (time (NULL) );
   int length = rand() % 6 + 4;
   
   char name[ MAX_LENGTH ]; 
   name[0] = 'A' + ( rand() % 26 );  
   
   for ( int i = 1; i < length ; i++ ) {
      name[i] = 'a' + ( rand() % 26 );
   }

   return name;
}


int main() {
	cout << "TEST: ANIMAL FACTORY" << endl;

	Animal animalTest = AnimalFactory::getRandomAnimal();
   cout << "Random Name = " << animalTest.name << endl;
	cout << "Random Color = " << animalTest.colorName( animalTest.hairColor ) << endl;
	cout << "Random Gender = " << animalTest.genderName( animalTest.gender ) << endl;
	

	return 0;
}
*/


#include <iostream>
#include <random>
#include "animal.hpp"

int main (){
  std::random_device rd;
Animal animalTest = AnimalFactory::getRandomAnimal();
  std::cout << rd() << std::endl;
cout << "Random Color = " << animalTest.colorName( animalTest.hairColor ) << endl;
  return 0;
}







