///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "animal.hpp"

namespace animalfarm {

class Bird : public Animal {
public:
   enum Color  featherColor;
   bool        isMigratory;
   virtual const string speak();
   void printInfo();
};

} // namespace animalfarm
