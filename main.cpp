///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
//#include <random>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
#define SIZE_OF_FARM (30)
#define NUMBER_OF_ANIMALS (25)
	
//	array <Animal, SIZE_OF_FARM> animalArray;
   std::array<Animal*, SIZE_OF_FARM> animalArray;
   animalArray.fill(NULL);

	for( int i = 0 ; i < NUMBER_OF_ANIMALS ; i++ ) {
		animalArray[i] = AnimalFactory::getRandomAnimal();
	}
	
	cout << endl;
	cout << "Array of Animals: " << endl;
	cout << "   Is it empty: " << animalArray.empty()<< endl;
	cout << "   Number of elements: " << animalArray.size() << endl;
	cout << "   Max size: " << animalArray.max_size()<< endl;
	
	//cout << endl;
//	cout << "The array of animals sounds like this:: " << endl;
	/*
   for( auto i = animalArray.begin() ; i != animalArray.end() ; ++i ) {
		cout << "   " << i->speak() << endl;
	}
	*/

   for(Animal* animal : animalArray){
      if(animal == NULL){
         break;
      }
      cout << animal->speak() << endl;
   }
   for( int i = 0; i < 25; i++){
      delete animalArray[i];
   }
	//list <Animal, SIZE_OF_FARM> animalList;
   std::list<Animal*> animalList;

	for( int i = 0 ; i < NUMBER_OF_ANIMALS ; i++ ) {
		animalList.push_front( AnimalFactory::getRandomAnimal() );
	}
	
	cout << endl;
	cout << "List of Animals: " << endl;
	cout << "   Is it empty: " << animalList.empty() << endl;
	cout << "   Number of elements: " << animalList.size() << endl;
	cout << "   Max size: " << animalList.max_size() << endl;
	
	//cout << endl;
	//cout << "The list of animals sounds like this::" << endl;
	
/*   for( auto i = animalList.begin() ; i != animalList.end() ; ++i ) {
		cout << "   " << i->speak() << endl;
	}
*/
   for(Animal* animal:animalList){
      cout << animal->speak() <<endl;
   }

/*   animalArray.clear();
   animalList.clear();
   if (animalArray.empty && animalList.empty)
      cout << "Farewell Animals great and small" << endl;
*/
   while(!animalList.empty()){
      delete animalList.front();
      animalList.pop_front();
   }
	return 0;

}
/*
   Animal AnimalFactory::getRandomAnimal(){
      Animal* newAnimal = NULL;
       int i = getRandomInteger % 6  // This gives you a number from 0 to 5
       switch (i) {
         case 0: newAnimal = new Cat ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER ); 
         case 1: newAnimal = new Dog ( same stuff );
         case 2: newAnimal = new Nunu ( RANDOM_BOOL, RED, RANDOM_GENDER );
         case 3: newAnimal = new Aku ( RANDOM_WEIGHT, SILVER, RANDOM_GENDER ); 
         case 4: newAnimal = new Palila( RANDOM_NAME, YELLOW, RANDOM_GENDER );
         case 5: newAnimal = new Nene ( RANDOM_NAME, BROWN, RANDOM_GENDER ); 
       }
       return newAnimal;
   }
}*/
	/*Cat myCat( "Bella", Animal::getRandomColor(), FEMALE );
	myCat.printInfo();
	
   Dog myDog( "Arloe", WHITE, MALE );
   myDog.printInfo();
	
	Nunu myNunu( true, RED, FEMALE );
	myNunu.printInfo();
	
   Aku myAku( 15.0, SILVER, MALE );
   myAku.printInfo();
	
	Palila myPalila( "Kapi'olani Regional Park", YELLOW, FEMALE );
   myPalila.printInfo();
	
	Nene myNene( "2202-A-802.1", BROWN, FEMALE );
	myNene.printInfo();
	
#define SIZE_OF_FARM (6)
	
	Animal* myAnimals[ SIZE_OF_FARM ];

	for( int i = 0 ; i < SIZE_OF_FARM ; i++ ) {
		myAnimals[i] = NULL;
	}
	
	myAnimals[0] = &myCat;
   myAnimals[1] = &myDog;
   myAnimals[2] = &myNunu;
	myAnimals[3] = &myAku;
   myAnimals[4] = &myPalila;
	myAnimals[5] = &myNene;
	
	cout << endl;
	cout << "Here's what it sounds like around the farm:" << endl;
	
	for( int i = 0 ; i < SIZE_OF_FARM ; i++ ) {
		if( myAnimals[i] != NULL) {
			cout << "   " << myAnimals[i]->speak() << endl;
		}
	}
*/	
//	return 0;
//}
