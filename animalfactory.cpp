#include <iostream>
#include <string>
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "aku.hpp"

using namespace std;

namespace animalfarm {

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;

   int ranNum = rand() % 6;

   switch(ranNum) {

      case 0: newAnimal = new Cat( Cat::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());      break;
      case 1: newAnimal = new Dog( Dog::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());      break;
      case 2: newAnimal = new Nunu( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() );    break;
      case 3: newAnimal = new Aku( Animal::getRandomWeight(1.5,9.8), Animal::getRandomColor(), Animal::getRandomGender() );     break;
      case 4: newAnimal = new Palila( Animal::getRandomName(), YELLOW, Animal::getRandomGender());   break;
      case 5: newAnimal = new Nene( Animal::getRandomName(), BROWN, Animal::getRandomGender());     break;
   }

   return newAnimal;
}
}

